@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Access Denied</div>

                <div class="panel-body">
                    <p>You do not have permission to view the resource that you have requested.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
