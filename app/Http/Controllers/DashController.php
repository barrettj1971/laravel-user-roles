<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{

	public function index() {
		if(Auth::user()->role == 'admin') {
			return redirect('/dashboard');
		} 
		return redirect('/member_dashboard'); 
	}

    public function admin() {
    	return view('dashboard.admin_dash');
    }

    public function user() {
    	return view('dashboard.user_dash'); 
    }
}
