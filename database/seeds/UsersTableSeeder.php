<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'My Name',
            'email' => 'email@email.com',
            'password' => bcrypt('SomePassword1234'),
            'role' => 'admin'
        ]);
    }
}
