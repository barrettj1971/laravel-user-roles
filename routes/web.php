<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// admin only routes
Route::group(['middleware' => ['auth', 'admin']], function() {
	Route::get('/dashboard', 'DashController@admin'); 
});

// normal user only routes
Route::group(['middleware' => ['auth', 'user']], function() {
	Route::get('/member_dashboard', 'DashController@user'); 
});

// any logged in user
Route::group(['middleware' => ['auth']], function() {
	// main dashboard route that will route to correct admin area
	// based on role
	Route::get('/dash', 'DashController@index'); 
	// no permission for request
	Route::get('/denied', function() {
		return view('dashboard.denied'); 
	});
});

Auth::routes();
